Name:           ibus-libpinyin
Version:        1.15.7
Release:        1
Summary:        Intelligent Pinyin engine based on libpinyin for IBus
License:        GPLv2+
Group:          System Environment/Libraries
URL:            https://github.com/libpinyin/ibus-libpinyin
Source0:        http://downloads.sourceforge.net/libpinyin/ibus-libpinyin/%{name}-%{version}.tar.gz

Requires:       python3-gobject ibus >= 1.5.11
BuildRequires:  gcc-c++ gettext-devel intltool libtool pkgconfig sqlite-devel libuuid-devel lua-devel
BuildRequires:  python3-devel desktop-file-utils ibus-devel >= 1.5.11 libpinyin-devel >= 2.1.0
Requires:       libpinyin-data >= 1.5.91
Requires(post): sqlite

Obsoletes: ibus-pinyin < 1.4.0-17

%description
It includes a Chinese Pinyin input method and a Chinese ZhuYin (Bopomofo)
input method based on libpinyin for IBus.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-static \
           --with-python=python3 \
           --disable-boost
%{make_build}

%check
desktop-file-validate %{buildroot}%{_datadir}/applications/ibus-setup-libpinyin.desktop
desktop-file-validate %{buildroot}%{_datadir}/applications/ibus-setup-libbopomofo.desktop

%install
%{make_install}

%py_byte_compile %{__python3} $RPM_BUILD_ROOT%{_datadir}/ibus-libpinyin/setup

%find_lang %{name}

%post
[ -x %{_bindir}/ibus ] && \
  %{_bindir}/ibus write-cache --system &>/dev/null || :

%postun
[ -x %{_bindir}/ibus ] && \
  %{_bindir}/ibus write-cache --system &>/dev/null || :

%files -f %{name}.lang
%doc AUTHORS COPYING README
%dir %{_datadir}/ibus-libpinyin
%dir %{_datadir}/ibus-libpinyin/db
%{_datadir}/metainfo/*.appdata.xml
%{_datadir}/glib-2.0/schemas/*.gschema.xml
%{_datadir}/applications/ibus-setup-libpinyin.desktop
%{_datadir}/applications/ibus-setup-libbopomofo.desktop
%{_datadir}/ibus-libpinyin/icons
%{_datadir}/ibus-libpinyin/setup
%{_datadir}/ibus-libpinyin/base.lua
%{_datadir}/ibus-libpinyin/user.lua
%{_datadir}/ibus-libpinyin/network.txt
%{_datadir}/ibus-libpinyin/db/english.db
%{_datadir}/ibus-libpinyin/db/table.db
%{_datadir}/ibus/component/*
%{_libexecdir}/ibus-engine-libpinyin
%{_libexecdir}/ibus-setup-libpinyin


%changelog
* Tue Mar 05 2024 maqi <maqi@uniontech.com> - 1.15.7-1
- Upgrade to version 1.15.7

* Wed Nov 29 2023 wangqia <wangqia@uniontech.com> - 1.15.5-1
- Upgrade to version 1.15.5

* Mon Sep 04 2023 chenchen <chen_aka_jan@163.com> - 1.15.3-1
- Upgrade to version 1.15.3

* Sat Aug 08 2020 lingsheng <lingsheng@huawei.com> - 1.10.0-6
- Fix build with lua 5.4

* Tue Mar 10 2020 wangzhishun <wangzhishun1@huawei.com> - 1.10.0-5
- Package init
